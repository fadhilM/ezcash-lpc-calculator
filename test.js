const moment = require('moment');

const startDate = '2024-01-04';
const endDate = '2024-03-04';
const InstallmentDue = 675.00;

const startDate2 = '2024-03-05';
const endDate2 = '2024-05-20';

const fixedInterest = 8;
const paramEndDate = moment(endDate2);
const timeDiff = paramEndDate.diff(startDate2, 'days');
console.log(timeDiff);
console.log(fixedInterest / 100);
console.log(timeDiff / 365);
console.log(InstallmentDue);
const interestAmount = (fixedInterest / 100) * (timeDiff / 365) * InstallmentDue;

console.log('Interest Amount: ', interestAmount);